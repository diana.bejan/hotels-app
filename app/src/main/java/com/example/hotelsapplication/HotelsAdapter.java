package com.example.hotelsapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;

public class HotelsAdapter extends ArrayAdapter<Hotel> {
    public HotelsAdapter(@NonNull final Context context, @NonNull final List<Hotel> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final Hotel hotel = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_hotel, parent, false);
        }

        final TextView hotelName = (TextView) convertView.findViewById(R.id.text_hotel_name);
        hotelName.setText(hotel.getName());
        final Button addressButton = (Button) convertView.findViewById(R.id.button_hotel_address);
        addressButton.setText(hotel.getAddress());

        final Button websiteButton = (Button) convertView.findViewById(R.id.button_hotel_website);
        websiteButton.setText(hotel.getWebsite());
        final Button phoneButton = (Button) convertView.findViewById(R.id.button_hotel_phone);
        phoneButton.setText(hotel.getPhone());

        addressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Uri location = Uri.parse("geo:0,0?q=" + hotel.getWebsite());

                final Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(location);
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);

                }
            }
        });

        websiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Uri webpage = Uri.parse("https://" + hotel.getWebsite());
                final Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);

                }
            }
        });

        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + hotel.getPhone()));
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);

                }
            }
        });
        return convertView;
    }
}
