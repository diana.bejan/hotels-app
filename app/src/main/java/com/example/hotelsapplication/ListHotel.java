package com.example.hotelsapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListHotel extends AppCompatActivity {

    private final static int BUTTON_DIMENSIONS = 80;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_list_hotel);
        this.buildList();
        // setButtonDimensions();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        this.buildList();
    }

    public void openNewHotelActivity(final View view) {
        final Intent intent = new Intent(this, NewHotel.class);
        this.startActivity(intent);

    }


    private void buildList() {
        final HotelDB db = new HotelDB(this);
        final List<Hotel> hotels = db.get();
        final HotelsAdapter hotelsAdapter =
                new HotelsAdapter(this, hotels);
        final ListView listView = (ListView) findViewById(R.id.list_hotels);
        listView.setAdapter(hotelsAdapter);
    }

    private void setButtonDimensions() {
        final ImageButton openButton = (ImageButton) findViewById(R.id.button_open_new_hotel_activity);
        final float density = getResources().getDisplayMetrics().density;


        openButton.getLayoutParams().width = (int) (BUTTON_DIMENSIONS * density);
        openButton.getLayoutParams().height = (int) (BUTTON_DIMENSIONS * density);

    }
}
