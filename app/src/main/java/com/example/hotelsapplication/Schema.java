package com.example.hotelsapplication;

import android.provider.BaseColumns;

public final class Schema {

    private Schema() {}

    public static class HotelsTable implements BaseColumns{
        public static final String TABLE_NAME = "hotels";
        public static final String COLUMN_NAME_HOTELNAME = "name";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_WEBSITE = "website";
        public static final String COLUMN_NAME_PHONE = "phone";
    }
}
