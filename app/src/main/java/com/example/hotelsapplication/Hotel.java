package com.example.hotelsapplication;

public class Hotel {
    private String name, address, website, phone;

    public Hotel(final String name, final String address, final String website, final String phone) {
        this.name = name;
        this.address = address;
        this.website = website;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getWebsite() {
        return website;
    }

    public String getPhone() {
        return phone;
    }
}
