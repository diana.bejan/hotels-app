package com.example.hotelsapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class NewHotel extends AppCompatActivity {


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_hotel);
    }

    public void createHotelEntry(final View view) {
        final HotelDB database = new HotelDB(this);
        final String name = ((EditText) findViewById(R.id.hotel_name)).getText().toString();
        final String address = ((EditText) findViewById(R.id.hotel_address)).getText().toString();
        final String website = ((EditText) findViewById(R.id.hotel_webpage)).getText().toString();
        final String phone = ((EditText) findViewById(R.id.hotel_phone)).getText().toString();
        final Hotel hotel = new Hotel(name, address, website, phone);
        if (database.insert(hotel) != -1) {
            final Toast toast = Toast.makeText(this, "Hotel created", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}

