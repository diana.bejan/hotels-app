package com.example.hotelsapplication;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class HotelDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Hotels.db";
    private static final String CREATE_TABLE_SQL_QUERY = "CREATE TABLE " + Schema.HotelsTable.TABLE_NAME + "(" +
            Schema.HotelsTable._ID + " INTEGER PRIMARY KEY, " + Schema.HotelsTable.COLUMN_NAME_HOTELNAME +
            " TEXT, " + Schema.HotelsTable.COLUMN_NAME_ADDRESS + " TEXT, " + Schema.HotelsTable.COLUMN_NAME_WEBSITE +
            " TEXT, " + Schema.HotelsTable.COLUMN_NAME_PHONE + " TEXT);";

    public HotelDBHelper(@Nullable final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_SQL_QUERY);
        } catch (final SQLException e) {
            System.out.printf("Exception: Table may be already created: %s\n", e.getMessage());
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        // NOP
    }
}
