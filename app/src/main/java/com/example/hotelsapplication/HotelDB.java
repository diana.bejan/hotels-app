package com.example.hotelsapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class HotelDB {
    private HotelDBHelper dbHerper;

    public HotelDB(final Context context) {
        this.dbHerper = new HotelDBHelper(context);
    }

    public long insert(final Hotel hotel) {
        final SQLiteDatabase database = this.dbHerper.getWritableDatabase();

        final ContentValues values = new ContentValues();
        values.put(Schema.HotelsTable.COLUMN_NAME_HOTELNAME, hotel.getName());
        values.put(Schema.HotelsTable.COLUMN_NAME_PHONE, hotel.getPhone());
        values.put(Schema.HotelsTable.COLUMN_NAME_ADDRESS, hotel.getAddress());
        values.put(Schema.HotelsTable.COLUMN_NAME_WEBSITE, hotel.getWebsite());


        return database.insert(Schema.HotelsTable.TABLE_NAME, null, values);
    }

    public List<Hotel> get() {
        final SQLiteDatabase database = this.dbHerper.getReadableDatabase();
        final String[] columns = new String[]{Schema.HotelsTable.COLUMN_NAME_HOTELNAME,
                Schema.HotelsTable.COLUMN_NAME_ADDRESS,
                Schema.HotelsTable.COLUMN_NAME_WEBSITE,
                Schema.HotelsTable.COLUMN_NAME_PHONE};
        final Cursor cursor = database.query(Schema.HotelsTable.TABLE_NAME, columns, null, null, null, null, null);
        cursor.moveToFirst();
        return decodeHotels(cursor);
    }

    private List<Hotel> decodeHotels(final Cursor cursor) {
        final List<Hotel> hotels = new ArrayList<>();
        while (cursor.moveToNext()) {
            final String name = cursor.getString(cursor.getColumnIndex(Schema.HotelsTable.COLUMN_NAME_HOTELNAME));
            final String address = cursor.getString(cursor.getColumnIndex(Schema.HotelsTable.COLUMN_NAME_ADDRESS));
            final String website = cursor.getString(cursor.getColumnIndex(Schema.HotelsTable.COLUMN_NAME_WEBSITE));
            final String phone = cursor.getString(cursor.getColumnIndex(Schema.HotelsTable.COLUMN_NAME_PHONE));
            final Hotel hotel = new Hotel(name, address, website, phone);
            hotels.add(hotel);
            cursor.moveToNext();
        }
        return hotels;
    }
}